var daysInMonth = require('days-in-month');
var moment = require('moment-business-days');
var uuid = require('uuid/v4');

exports.generateDays = function (req) {
    var date = new Date(Date.parse(req.query.month));
    var month = date.getMonth() + 1;
    var year = date.getFullYear();
    var days = daysInMonth(date);
    var entries = [];

    for (i = 1; i <= days; i++) {
        var day = year + "-" + this.numTwoDigits(month) + "-" + this.numTwoDigits(i);
        var daysoff = req.query.daysoff;

        if (moment(day, 'YYYY-MM-DD').isBusinessDay() && !daysoff.includes(day)) {
            entries.push(
                {
                    day: day,
                    vehicle: req.query.vehicle,
                    route: req.query.route,
                    distance: req.query.distance,
                    purpose: req.query.purpose
                }
            );
        }
    }
    return entries;
};

exports.numTwoDigits = function (n) {
    return ("0" + n).slice(-2);
};

exports.generateUUID = function () {
    return uuid();
};

exports.saveFieldsInCookies = function (req, res) {
    res.cookie('month', req.query.month);
    res.cookie('vehicle', req.query.vehicle);
    res.cookie('route', req.query.route);
    res.cookie('distance', req.query.distance);
    res.cookie('purpose', req.query.purpose);
    res.cookie('daysoff', req.query.daysoff);
};