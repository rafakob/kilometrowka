// dependencies
var express = require('express');
var app = express();

// express config
app.use('/public', express.static(process.env.PWD + '/public'));
app.set('port', process.env.PORT || 5000);
app.set('views', __dirname + '/views');
app.set('view engine', 'pug');
app.locals.pretty = true;

// routes
var index = require('./routes/index');
var preview = require('./routes/preview');
var download = require('./routes/download');

app.get('/', index.show);
app.get('/preview', preview.show);
app.get('/download', download.csv);


// start
app.listen(app.get('port'), function () {
    console.log("Server listening at http://127.0.0.1:5000")
});
