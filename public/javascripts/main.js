$(document).ready(function () {
    $('#monthpicker').datepicker({
        format: "MM yyyy",
        viewMode: "months",
        minViewMode: "months",
        autoclose: true,
    });

    $('#daypicker').datepicker({
        format: "yyyy-mm-dd",
        autoclose: false,
        multidate: true,
        maxViewMode: 'month',
        updateViewDate: false,
    });

    $('#monthpicker').datepicker('setDate', new Date(getCookie('month')));
    $('#daypicker').datepicker('setDates', getCookie('daysoff').split(','));

    $('#month').val(getCookie('month'));
    $('#vehicle').val(getCookie('vehicle'));
    $('#route').val(getCookie('route'));
    $('#distance').val(getCookie('distance'));
    $('#purpose').val(getCookie('purpose'));
    $('#daysoff').val(getCookie('daysoff'));

    function getCookie(cname) {
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }
});