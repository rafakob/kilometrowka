var page = 'pages/preview';
var commons = require('./../modules/commons.js');

exports.show = function (req, res) {
    commons.saveFieldsInCookies(req, res);

    res.render(page, {dates: commons.generateDays(req)});
    res.end();
};