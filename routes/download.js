var commons = require('./../modules/commons.js');
var fs = require('fs');
var mkdirp = require('mkdirp');
var rimraf = require('rimraf');
var json2csv = require('json2csv');


exports.csv = function (req, res) {
    commons.saveFieldsInCookies(req, res);

    var fields = ['day', 'vehicle', 'route', 'distance', 'purpose'];
    var csv = json2csv({data: commons.generateDays(req), fields: fields, del: ';', hasCSVColumnTitle: false});
    var dir = 'generated/' + commons.generateUUID();
    var filename = 'Kilometrowka.csv';
    var path = dir + '/' + filename;
    var file = __dirname + '/../' + path;

    mkdirp(dir, function () {
        fs.writeFile(path, csv, function (err) {
            if (err) throw err;
            res.download(file, function () {
                if (err) throw err;
                rimraf(dir, function () {
                    if (err) throw err;
                    res.end();
                });
            });
        });
    });
};